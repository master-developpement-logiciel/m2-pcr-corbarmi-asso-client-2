package m2dl.corbarmi.client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public abstract class AssoClientGui implements ActionListener {
	
	private JTextField cp;
	private final DefaultTableModel model;

	private JTable table;
	
	public void actionPerformed(ActionEvent e) {
		updateResultsTable();
	}
	
	/** 
	 * Creates new form AssoClientGui
	 */
	public AssoClientGui() {

		cp = new JTextField(5);
		cp.addActionListener(this);
		model = new DefaultTableModel(new String[][] {}, new String[] { "AssoID", "Name", "Description" });
		table = new JTable(model);
	}
	
	private void createAndShowGUI() {
		JFrame frame = new JFrame("Association lookup");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(cp);
		JButton b = new JButton("Ok");
		b.addActionListener(this);
		frame.add(b);
		frame.add(new JScrollPane(table));
		frame.setLayout(new FlowLayout());
		frame.pack();
		frame.setVisible(true);
	}

	
	public void run() {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
	}

	/**
	 * Called every time the Enter key is typed in the CP field
	 */
	protected abstract void updateResultsTable();

	/**
	 * @return the content of the CP field
	 */
	protected String getCurrentCP() {
		return cp.getText();
	}

	/**
	 * Remove any entry in the table
	 */
	protected void clearTable() {
		model.setRowCount(0);
	}

	/**
	 * Add an entry in the table
	 * 
	 * @param id The ID of the association
	 * @param name The name of the association
	 * @param description The description of the association
	 */
	protected void addTableItem(String id, String name, String description) {
		model.insertRow(0,new String[]{id,name,description});
	}
}
